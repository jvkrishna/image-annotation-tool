var express = require('express');
var router = express.Router();
var authenticator = require('../config/authenticator');

/* GET users listing. */
router.get('/', authenticator.authentication, function(req, res, next) {
  res.send('respond with a resource');
});

module.exports = router;
