# VGG Image Annotator

VGG Image Annotator (via) is an open source project developed at the 
[Visual Geometry Group](http://www.robots.ox.ac.uk/~vgg/) and released under 
the BSD-2 clause license. This work is supported by EPSRC programme grant 
Seebibyte: Visual Search for the Era of Big Data ([EP/M013774/1](http://www.seebibyte.org/index.html)).
Visit the [VGG software page](http://www.robots.ox.ac.uk/~vgg/software/via/) for more details.

### Features:
  * based solely on HTML, CSS and Javascript (no dependecies on any javascript libraries)
  * can be used offline (complete application packaged in a single html file of size < 200KB)
  * requires nothing more than a modern web browser (tested on firefox and chrome)
  * supports following region shapes: rectangle, circle, ellipse, polygon
  * supports multiple attributes for each image region
  * import (and export) of region data from (to) text file in csv and json format
  * hundreds of images can be loaded and annotated with any performance degradation

## Downloads
 * [via.html.zip](http://www.robots.ox.ac.uk/~vgg/software/via/downloads/via.html.zip) : the VGG Image Annotator application (< 200KB)
 * [via.html](http://www.robots.ox.ac.uk/~vgg/software/via/downloads/via.html) : online version of the application
 * [via demo](http://www.robots.ox.ac.uk/~vgg/software/via/demo/via_demo.html) : live demo (with preloaded images and regions)
 * [https://gitlab.com/vgg/via/](https://gitlab.com/vgg/via/) : source code repository

## Help:
Software bug reports and feature requests should be [submitted here](https://gitlab.com/vgg/via/issues/new).
For all other queries, please contact [Abhishek Dutta](mailto:adutta@robots.ox.ac.uk).
